# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView, MatchMixin
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Template', 'Product', 'WarehouseService']


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    warehouse_services = fields.One2Many('stock.warehouse.service',
        'product', 'Warehouse services', states={
            'invisible': (Eval('type') == 'service')
        }, depends=['type'])


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'


class WarehouseService(ModelSQL, ModelView, MatchMixin):
    '''Warehouse Service'''
    __name__ = 'stock.warehouse.service'

    product = fields.Many2One('product.template', 'Product',
        required=True, select=True,
        domain=[('type', '!=', 'service')])
    service = fields.Many2One('product.product', 'Service',
        required=True, select=True,
        domain=[('template.type', '=', 'service')])
    type_ = fields.Selection([
        ('storage', 'Storage'),
        ('stock', 'Stock Quantity'),
        ('production', 'Production')], 'Type', required=True)
    warehouse = fields.Many2One('stock.location', 'Warehouse',
        domain=[('type', '=', 'warehouse')], select=True)

    @staticmethod
    def default_type_():
        return 'storage'

    def match(self, pattern):
        _pattern = pattern.copy()
        _ = _pattern.pop('warehouse')

        value = super(WarehouseService, self).match(_pattern)

        if value and 'warehouse' in pattern:
            if self.warehouse and self.warehouse.id != pattern['warehouse']:
                return False
        return value
